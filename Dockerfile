FROM python:rc-alpine3.12
RUN apk add -U git

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY . .

RUN pip install -r requirements.txt

RUN ["chmod", "+x", "./run.sh"]

CMD ./run.sh